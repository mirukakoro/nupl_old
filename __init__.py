import nupl

if __name__ == '__main__':
  print('''_  _      ___ _
| \| |_  _| _ \ |
| :) | || |  _/ |__  By ColourDelete
|_|\_|\_,_|_| |____| https://gitlab.com/colourdelete/nupl
Known Errors:
* Quotes don't get treated properly in input src
* Newlines don't get treated probably in src header''')
  import argparse

  parser = argparse.ArgumentParser(prog='logulator')
  parser.add_argument('-mmp', '--meta-model-path', dest='mmp', help='Meta Model Path')
  parser.add_argument('-ms', '--model-str', dest='ms', help='Model String')
  parser.add_argument('-sp', '--src-path', dest='sp', help='Output Source Path')
  parser.add_argument('-sh', '--src-header', dest='sh', help='Output Source Header')
  parser.add_argument('-i', '--intp', '--interpret', dest='intp', help='Interpret', action='store_true')
  parser.add_argument('-emm', '--exp-mm', '--export-meta-model', dest='intp', help='Export Meta Model dot', action='store_true')
  parser.add_argument('-em', '--exp-m', '--export-model', dest='intp', help='Export Model dot', action='store_true')
  parser.add_argument('-p', '--pour', '--pour-into-mold', dest='intp', help='Pour into Mold', action='store_true')
  parser.add_argument('-es', '--exp-s', '--export-source', dest='intp', help='Export Source', action='store_true')
  args = parser.parse_args()
  inst = nupl.parser.Parser(args.mmp, args.ms, args.sp, args.sh)
  inst.do_it(True, True, True, True, True)