# NuPL

A new re.match("*").

## Introduction

Python is nice with its libraries, syntax, etc, but it's slow. NuPL aims to bring the speed of compiled languages, but also the libraries of interpreted ones. The goal is to be able to access Python, C, C++, Java libraries and still be only 5~10x slower than C.

## Details

| Language   | Major Libraries  |
| ---------- | ---------------- |
| Python     | TF, Requests     |
| C(++)      | OpenCV           | 
| JavaScript | React, AngularJS | 

## Examples

WIP