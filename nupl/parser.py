import os
from textx import *
from textx.export import *
import data


class Parser(object):
  
  def __init__(self, meta_model_path='grammar.tx', model_str=None, src_path='output.py', src_formulas=data.src_formulas, mold_type='python3'):
    self.tree = []
    self.meta_model_path = os.path.abspath(meta_model_path)
    if not model_str:
      with open('program.nupl', 'r') as file:
        model_str = file.read()
    self.model_str = model_str
    self.mm = None
    self.model = None
    self.mold_type = mold_type
    self.mold_types = ['c', 'c++', 'c#', 'java', 'python3', 'llvm_ast', 'nupl_ast']
    self.mold = None
    self.raw_src = '' # Output lang src
    self.src = ''
    self.src_path = src_path
    self.src_formula = src_formulas[mold_type]
    self.str = ''
  
  def __str__(self):
    return self.str
  
  def _update_str(self):
    result = ''
    for thing in self.tree:
      if thing['type'] == 'func':
        result += 'Function   "{}" with {}\n'\
          .format(thing['callee'], thing['args'])
      elif thing['type'] == 'defi':
        result += 'Definition {} is {}\n'\
          .format(thing['a'], thing['b'])
      elif thing['type'] == 'stat':
        result += 'Statement  {}, {} causes {}\n'\
          .format(thing['callee'], thing['checker'], thing['effect'])
    self.str = result
  
  def _interpret_object_raw(self, obj):
    result = {}
    result['raw'] = []
    for obj in obj.a:
      result['raw'].append(obj)
    result['type'] = 'object'
    return result

  def _interpret_object(self, obj):
    raw = self._interpret_object_raw(obj)
    if raw['type'] == 'obj func':
      result = self._interpret_function(raw['raw'])
    elif raw['type'] == 'obj defi':
      result = self._interpret_definition(raw['raw'])
    elif raw['type'] == 'obj coll': # TODO: Support for coll, str, id, int, float, bool
      result = ''.format()
    elif raw['type'] == 'obj str':
      result = ''.format()
    elif raw['type'] == 'obj id':
      result = ''.format()
    elif raw['type'] == 'obj int':
      result = ''.format()
    elif raw['type'] == 'obj float':
      result = ''.format()
    elif raw['type'] == 'obj bool':
      result = ''.format()
    return result
  
  def _interpret_function(self, c):
    result = {}
    result['type'] = 'func'
    result['callee'] = c.callee
    result['args'] = []
    for arg in c.args.arg:
      if arg.__class__.__name__ == 'UnnamedArgument':
        result['args'].append(arg.a.a[0])
      elif arg.__class__.__name__ == 'NamedArgument':
        result['args'].append([arg.a.a[0], arg.b.a[0]])
    return result
  
  def _interpret_definition(self, c):
    result = {}
    result['type'] = 'defi'
    try:
      result['a'] = c.a.a
    except AttributeError:
      result['a'] = str(c.a)
    try:
      result['b'] = c.a.b
    except AttributeError:
      result['b'] = str(c.b)
    return result
  
  def _interpret_statement(self, c):
    result = {}
    result['type'] = 'stat'
    result['callee'] = c.callee
    result['checker'] = []
    for checker in c.checker:
      result_child = []
      result_child['checker_a'] = checker.a
      result_child['checker_b'] = checker.b
      result_child['checker_sign'] = checker.sign
      result['checker'].append(result_child)
    result['effect'] = c.effect.objs
    for checker in c.checker:
      result.append(self._interpret_statement_child(checker))
    return result

  def _interpret(self):
    for c in self.model.commands:
      if c.__class__.__name__ == 'Statement':
        self.tree.append(self._interpret_statement(c))
      elif c.__class__.__name__ == 'Function':
        self.tree.append(self._interpret_function(c))
      elif c.__class__.__name__ == 'Definition':
        self.tree.append(self._interpret_definition(c))
  
  def export_meta_model(self):
    metamodel_export(self.mm, os.path.abspath('grammar.dot'))
    return [True, None]
  
  def export_model(self):
    model_export(self.model, os.path.abspath('program.dot'))
    return [True, None]
  
  def interpret(self):
    self.mm = metamodel_from_file(self.meta_model_path, debug=False)
    try:
      self.model = self.mm.model_from_str(self.model_str)
      # self.model = self.mm.model_from_file(os.path.abspath('program.nupl'))
    except TextXSyntaxError as err:
      print('Syntax Error @ {}:{}'.format(err.line, err.col))
      print('{}'.format(err.message))
      return [False, err]
    self._interpret()
    self._update_str()
    return [True, None]
  
  def pour(self):
    self.raw_src = ''
    if self.mold_type == 'python3':
      for i in range(len(self.tree)):
        thing = self.tree[i]
        if thing['type'] == 'func':
          callee = thing['callee']
          args = ''
          for arg in thing['args']:
            if type(arg) == type(''):
              args += '{}, '.format(arg)
            elif type(arg) == type([]):
              args += '{} = {}, '.format(arg[0], arg[1])
          self.raw_src += '{}({})\n'.format(callee, args)
        if thing['type'] == 'defi':
          a = thing['a']
          b = thing['b']
          self.raw_src += '{} = {}\n'.format(a, b)
    elif self.mold_type == 'c++':
      for i in range(len(self.tree)):
        thing = self.tree[i]
        if thing['type'] == 'func':
          callee = thing['callee']
          args = ''
          for arg in thing['args']:
            if type(arg) == type(''):
              args += '{}, '.format(arg)
            elif type(arg) == type([]):
              args += '{} = {}, '.format(arg[0], arg[1])
          self.raw_src += '{}({})\n'.format(callee, args)
    self.src = self.src_formula.replace('<[{(src)}]>', self.raw_src)
    return [True, None]
  
  def export_src(self):
    with open(self.src_path, 'w') as file:
      file.write(self.raw_src)
    return [True, None]
  
  def do_it(self, intp=True, exp_mm=False, exp_m=False, pour=True, exp_src=True):  # My naming skills :)
    cont = True
    err = None
    if cont and intp:    cont, err = self.interpret()
    if cont and exp_mm:  cont, err = self.export_meta_model()
    if cont and exp_m:   cont, err = self.export_model()
    if cont and pour:    cont, err = self.pour()
    if cont and exp_src: cont, err = self.export_src()


def main(debug=False):
  print('NuPL')
  program = Parser(mold_type='c++')
  program.do_it(True, True, True, True)
  print('Output')
  print(program)


if __name__ == "__main__":
  main()