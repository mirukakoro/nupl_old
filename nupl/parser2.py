from os.path import join, dirname
from textx import metamodel_from_file
from textx.export import metamodel_export, model_export


class Robot(object):

    def __init__(self):
      pass

    def interpret(self, model):

        # model is an instance of Program
        for c in model.commands:
            print(c.__class__.__name__)


def main(debug=False):

    this_folder = dirname(__file__)

    robot_mm = metamodel_from_file(join(this_folder, 'grammar.tx'), debug=False)
    metamodel_export(robot_mm, join(this_folder, 'robot_meta.dot'))

    robot_model = robot_mm.model_from_file(join(this_folder, 'program.nupl'))
    model_export(robot_model, join(this_folder, 'program.dot'))
    print(type(robot_model))

    robot = Robot()
    robot.interpret(robot_model)


if __name__ == "__main__":
    main()